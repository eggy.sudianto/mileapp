<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/package/{id?}', 'Api\HomeController@index')->name('package.index');
Route::post('/package', 'Api\HomeController@store')->name('package.store');
Route::put('/package/{id}', 'Api\HomeController@update');
Route::patch('/package/{id}', 'Api\HomeController@update');
Route::delete('/package/{id}', 'Api\HomeController@delete');
