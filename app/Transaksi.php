<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer_attribute;
use App\Connote;
use App\Customer;
use App\Koli_data;
use App\Custom_field;
use App\Currentlocation;
use DB;

class Transaksi extends Model
{

    protected $hidden = [
        'customer_attributes_id',"origin_data_id","destination_data_id","custom_fields_id","current_locations_id"
    ];

    protected $fillable = [
        'transaction_id','customers_id','transaction_amount','transaction_discount','transaction_additional_field','transaction_payment_type','transaction_state','transaction_code','transaction_order','location_id','organization_id','transaction_payment_type_name','transaction_cash_amount','transaction_cash_change','customer_attributes_id','origin_data_id','destination_data_id','custom_fields_id','current_locations_id','connotes_id'
    ];

    public function customer_attribute(){
        return $this->hasMany(Customer_attribute::class, 'customer_attributes_id' , 'customer_attributes_id')
                    ->select('customer_attributes_id','nama_sales','top','jenis_pelanggan');
    }


    public function connote(){
        return $this->hasMany(Connote::class, 'connotes_id' , 'connotes_id')->with('history')
                    ->select('connotes_id','connote_number','connote_service','connote_service_price','connote_amount','connote_code','connote_booking_code','connote_order','connote_state','connote_state_id','zone_code_from','zone_code_to','surcharge_amount','transaction_id','actual_weight','volume_weight','chargeable_weight','created_at','updated_at','organization_id','location_id','connote_total_package','connote_surcharge_amount','connote_sla_day','location_name','location_type','source_tariff_db','id_source_tariff','pod');
    }

    public function origin_data(){
        return $this->hasMany(Customer::class, 'customers_id' , 'origin_data_id')
                    ->select('customers_id','customers_name','customer_address','customer_email','customer_phone','customer_address_detail','customer_zip_code','zone_code','organization_id','location_id');
    }

    public function destination_data(){
        return $this->hasMany(Customer::class, 'customers_id' , 'destination_data_id')
                    ->select('customers_id','customers_name','customer_address','customer_email','customer_phone','customer_address_detail','customer_zip_code','zone_code','organization_id','location_id');
    }

    public function koli_data(){
        return $this->hasMany(Koli_data::class, 'connotes_id' , 'connotes_id')->with('koli_surcharge')->with('koli_custom_field')
                    ->select('koli_length','awb_url','created_at','koli_chargeable_weight','koli_width','koli_height','updated_at','koli_description','koli_formula_id','connotes_id','koli_volume','koli_weight','koli_id','koli_code','koli_custom_fields_id');
    }

    public function custom_field(){
        return $this->hasMany(Custom_field::class, 'custom_fields_id' , 'custom_fields_id')
                    ->select('custom_fields_id','catatan_tambahan');
    }

    public function currentLocation(){
        return $this->hasMany(Currentlocation::class, 'current_locations_id' , 'current_locations_id')
                    ->select('current_locations_id','name','code','type');
    }
}
