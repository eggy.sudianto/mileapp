<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Custom_field extends Model
{
    protected $hidden = [
        'custom_fields_id'
    ];
}
