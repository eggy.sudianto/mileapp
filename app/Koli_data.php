<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\koli_custom_field;
use App\koli_surcharge;

class Koli_data extends Model
{

	protected $hidden = [
        'koli_custom_fields_id',
    ];

    public function koli_custom_field(){
        return $this->hasMany(koli_custom_field::class, 'koli_custom_fields_id' , 'koli_custom_fields_id');
    }

    public function koli_surcharge(){
        return $this->hasMany(koli_surcharge::class, 'koli_surcharges_id' , 'koli_surcharges_id');
    }
}
