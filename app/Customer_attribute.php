<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_attribute extends Model
{
    protected $hidden = [
        'customer_attributes_id',
    ];
}
