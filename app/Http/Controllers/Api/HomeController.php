<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaksi;
use App\Customer_attribute;
use App\User;
use App\Connote;
use DB;

class HomeController extends Controller
{
    public function index($id=null)
	{
		$data = Transaksi::from("transaksis as trx")
							->select("trx.transaction_id","cus.customers_name","cus.customers_code","trx.transaction_amount","trx.transaction_discount","trx.transaction_additional_field","trx.transaction_payment_type","trx.transaction_state","trx.transaction_code","trx.transaction_order","trx.location_id","trx.organization_id","trx.created_at","trx.updated_at","trx.transaction_payment_type_name","trx.transaction_cash_amount","trx.transaction_cash_change","trx.connotes_id","trx.customer_attributes_id","trx.origin_data_id","trx.destination_data_id","trx.custom_fields_id","trx.current_locations_id")
							->join("customers as cus","cus.customers_id","trx.customers_id");
		if($id != null){
			$data = $data->where("trx.transaction_id",$id);
		}

		$data = $data->with('customer_attribute')->with('connote')->with('origin_data')->with('destination_data')->with('koli_data')->with('custom_field')->with('currentLocation')->get();
		return \Response::json($data);
	} 

	public function store(Request $request){
		try {
			$saves = Connote::create([
				'connotes_id' => request('connotes_id'),
				'connote_number' => 1,
				'connote_service' => 'ECO',
				'connote_service_price' => 70700,
				'connote_amount' => 70700,
				'connote_code' => 'AWB00100209082020',
				'connote_booking_code' => null,
				'connote_order' => 326931,
				'connote_state' => 'PAID',
				'connote_state_id' => '2',
				'zone_code_from' => 'CGKFT',
				'zone_code_to' => 'SMG',
				'surcharge_amount' => null,
				'transaction_id' => request('transaction_id'),
				'actual_weight' => 20,
				'volume_weight' => 0,
				'chargeable_weight' => 20,
				'organization_id' => 6,
				'location_id' => '5cecb20b6c49615b174c3e74',
				'connote_total_package' => 3,
				'connote_surcharge_amount' => 0,
				'connote_sla_day' => 4,
				'location_name' => 'Hub Jakarta Selatan',
				'location_type' => 'HUB',
				'source_tariff_db' => 'tariff_customers',
				'id_source_tariff' => 1576868,
				'pod' => '',
				'histories_id' => null,
			]);

			$save = Transaksi::create([
				'transaction_id' => request('transaction_id'),
				'customers_id' => request('customers_id'),
				'transaction_amount' => request('transaction_amount'),
				'transaction_discount' => request('transaction_discount'),
				'transaction_additional_field' => request('transaction_additional_field'),
				'transaction_payment_type' => request('transaction_payment_type'),
				'transaction_state' => request('transaction_state'),
				'transaction_code' => request('transaction_code'),
				'transaction_order' => request('transaction_order'),
				'location_id' => request('location_id'),
				'organization_id' => request('organization_id'),
				'transaction_payment_type_name' => request('transaction_payment_type_name'),
				'transaction_cash_amount' => request('transaction_cash_amount'),
				'transaction_cash_change' => request('transaction_cash_change'),
				'customer_attributes_id' => request('customer_attributes_id'),
				'origin_data_id' => request('origin_data_id'),
				'destination_data_id' => request('destination_data_id'),
				'custom_fields_id' => request('custom_fields_id'),
				'current_locations_id' => request('current_locations_id'),
				'connotes_id' => request('connotes_id'),
			]);

			

			if ($save and $saves){
				echo "Berhasil Memasukkan Data \n\n";
			}else{
				echo "Gagal Memasukkan Data \n\n";
			}
			print_r(json_encode($request->all(), JSON_PRETTY_PRINT));
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e ;
        } catch (PDOException $e) {
            echo $e;
        }   
	} 


	public function update(Request $request, $id){
		try {
			$save = DB::table('transaksis')
			->where('transaction_id', $id)
			->update($request->all());

			if ($save){
				echo "Berhasil Mengubah Data \n\n";
			}else{
				echo "Gagal Mengubah Data \n\n";
			}
			print_r(json_encode($request->all(), JSON_PRETTY_PRINT));
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e ;
        } catch (PDOException $e) {
            echo $e;
        }   
	} 

	public function delete(Request $request, $id){
		try {
			$delete = DB::table('transaksis')
			->where('transaction_id', $id)
			->delete();

			if ($delete){
				echo "Berhasil Menghapus Data Transaksi : ".$id;
			}else{
				echo "Gagal Menghapus Data Transaksi : ".$id;
			}

        } catch (\Illuminate\Database\QueryException $e) {
            echo $e ;
        } catch (PDOException $e) {
            echo $e;
        }   
	} 
}
