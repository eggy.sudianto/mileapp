<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentLocation extends Model
{
    protected $hidden = [
        'current_locations_id'
    ];
}
