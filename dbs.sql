/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.13-MariaDB : Database - mileapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mileapp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `mileapp`;

/*Table structure for table `connotes` */

DROP TABLE IF EXISTS `connotes`;

CREATE TABLE `connotes` (
  `connotes_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connote_number` int(11) DEFAULT NULL,
  `connote_service` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connote_service_price` int(11) NOT NULL DEFAULT 0,
  `connote_amount` int(11) NOT NULL DEFAULT 0,
  `connote_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connote_booking_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connote_order` int(10) unsigned NOT NULL,
  `connote_state` enum('PAID','UNPAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connote_state_id` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zone_code_from` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zone_code_to` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surcharge_amount` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_weight` int(11) NOT NULL DEFAULT 0,
  `volume_weight` int(11) NOT NULL DEFAULT 0,
  `chargeable_weight` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `location_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connote_total_package` int(11) NOT NULL DEFAULT 0,
  `connote_surcharge_amount` int(11) NOT NULL DEFAULT 0,
  `connote_sla_day` int(11) NOT NULL DEFAULT 0,
  `location_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_type` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_tariff_db` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_source_tariff` int(11) NOT NULL,
  `pod` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `histories_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`connotes_id`),
  KEY `connotes_histories_id_index` (`histories_id`),
  CONSTRAINT `connotes_histories_id_foreign` FOREIGN KEY (`histories_id`) REFERENCES `histories` (`histories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `connotes` */

insert  into `connotes`(`connotes_id`,`connote_number`,`connote_service`,`connote_service_price`,`connote_amount`,`connote_code`,`connote_booking_code`,`connote_order`,`connote_state`,`connote_state_id`,`zone_code_from`,`zone_code_to`,`surcharge_amount`,`transaction_id`,`actual_weight`,`volume_weight`,`chargeable_weight`,`created_at`,`updated_at`,`organization_id`,`location_id`,`connote_total_package`,`connote_surcharge_amount`,`connote_sla_day`,`location_name`,`location_type`,`source_tariff_db`,`id_source_tariff`,`pod`,`histories_id`) values 
('f70670b1-c3ef-4caf-bc4f-eefa702092ed',1,'ECO',70700,70700,'AWB00100209082020',NULL,326931,'PAID','2','CGKFT','SMG',NULL,'d0090c40-539f-479a-8274-899b9970bddc',20,0,20,'2020-08-26 09:29:58','2020-08-26 09:30:00',6,'5cecb20b6c49615b174c3e74',3,0,4,'Hub Jakarta Selatan','HUB','tariff_customers',1576868,'',NULL);

/*Table structure for table `current_locations` */

DROP TABLE IF EXISTS `current_locations`;

CREATE TABLE `current_locations` (
  `current_locations_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`current_locations_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `current_locations` */

insert  into `current_locations`(`current_locations_id`,`name`,`code`,`type`,`created_at`,`updated_at`) values 
(1,'Hub Jakarta Selatan','JKTS01','Agent','2020-08-26 09:21:56','2020-08-26 09:21:58');

/*Table structure for table `custom_fields` */

DROP TABLE IF EXISTS `custom_fields`;

CREATE TABLE `custom_fields` (
  `custom_fields_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catatan_tambahan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`custom_fields_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `custom_fields` */

insert  into `custom_fields`(`custom_fields_id`,`catatan_tambahan`,`created_at`,`updated_at`) values 
(1,'JANGAN DI BANTING \\/ DI TINDIH','2020-08-26 09:22:22','2020-08-26 09:22:24');

/*Table structure for table `customer_attributes` */

DROP TABLE IF EXISTS `customer_attributes`;

CREATE TABLE `customer_attributes` (
  `customer_attributes_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_sales` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_pelanggan` enum('B2B','B2C','C2C') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`customer_attributes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `customer_attributes` */

insert  into `customer_attributes`(`customer_attributes_id`,`nama_sales`,`top`,`jenis_pelanggan`,`created_at`,`updated_at`) values 
(1,'Radit Fitrawikarsa','14 Hari','B2B','2020-08-26 09:22:55','2020-08-26 09:22:57');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `customers_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customers_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customers_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_address_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_zip_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zone_code` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` int(11) NOT NULL,
  `location_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `customers` */

insert  into `customers`(`customers_id`,`customers_name`,`customers_code`,`customer_address`,`customer_email`,`customer_phone`,`customer_address_detail`,`customer_zip_code`,`zone_code`,`organization_id`,`location_id`,`created_at`,`updated_at`) values 
(1,'PT. NARA OKA PRAKARSA',NULL,'JL. KH. AHMAD DAHLAN NO. 100, SEMARANG TENGAH 12420','info@naraoka.co.id','024-1234567',NULL,'12420','CGK',6,'5cecb20b6c49615b174c3e74','2020-08-26 09:26:22','2020-08-26 09:26:24'),
(2,'PT AMARIS HOTEL SIMPANG LIMA',NULL,'JL. KH. AHMAD DAHLAN NO. 01, SEMARANG TENGAH',NULL,'0248453499','KOTA SEMARANG SEMARANG TENGAH KARANGKIDUL','50241','SMG',6,'5cecb20b6c49615b174c3e74','2020-08-26 09:27:13','2020-08-26 09:27:16'),
(3,'PT. AMARA PRIMATIGA','1678593',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL);

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `histories` */

DROP TABLE IF EXISTS `histories`;

CREATE TABLE `histories` (
  `histories_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `histories` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`histories_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `histories` */

insert  into `histories`(`histories_id`,`histories`,`created_at`,`updated_at`) values 
(1,'tes','2020-08-26 09:23:36','2020-08-26 09:23:38');

/*Table structure for table `koli_custom_fields` */

DROP TABLE IF EXISTS `koli_custom_fields`;

CREATE TABLE `koli_custom_fields` (
  `koli_custom_fields_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `awb_sicepat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_barang` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`koli_custom_fields_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `koli_custom_fields` */

insert  into `koli_custom_fields`(`koli_custom_fields_id`,`awb_sicepat`,`harga_barang`,`created_at`,`updated_at`) values 
(1,NULL,NULL,'2020-08-26 09:23:22','2020-08-26 09:23:23');

/*Table structure for table `koli_datas` */

DROP TABLE IF EXISTS `koli_datas`;

CREATE TABLE `koli_datas` (
  `koli_datas_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `koli_length` int(11) NOT NULL DEFAULT 0,
  `awb_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `koli_chargeable_weight` int(11) NOT NULL DEFAULT 0,
  `koli_width` int(11) NOT NULL DEFAULT 0,
  `koli_surcharges_id` int(10) unsigned DEFAULT NULL,
  `koli_height` int(11) NOT NULL DEFAULT 0,
  `koli_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `koli_formula_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connotes_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `koli_volume` int(11) NOT NULL DEFAULT 0,
  `koli_weight` int(11) NOT NULL DEFAULT 0,
  `koli_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `koli_custom_fields_id` int(10) unsigned NOT NULL,
  `koli_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`koli_datas_id`),
  KEY `koli_datas_koli_surcharges_id_index` (`koli_surcharges_id`),
  KEY `koli_datas_connotes_id_index` (`connotes_id`),
  KEY `koli_datas_koli_custom_fields_id_index` (`koli_custom_fields_id`),
  CONSTRAINT `koli_datas_connotes_id_foreign` FOREIGN KEY (`connotes_id`) REFERENCES `connotes` (`connotes_id`),
  CONSTRAINT `koli_datas_koli_custom_fields_id_foreign` FOREIGN KEY (`koli_custom_fields_id`) REFERENCES `koli_custom_fields` (`koli_custom_fields_id`),
  CONSTRAINT `koli_datas_koli_surcharges_id_foreign` FOREIGN KEY (`koli_surcharges_id`) REFERENCES `koli_surcharges` (`koli_surcharges_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `koli_datas` */

insert  into `koli_datas`(`koli_datas_id`,`koli_length`,`awb_url`,`created_at`,`updated_at`,`koli_chargeable_weight`,`koli_width`,`koli_surcharges_id`,`koli_height`,`koli_description`,`koli_formula_id`,`connotes_id`,`koli_volume`,`koli_weight`,`koli_id`,`koli_custom_fields_id`,`koli_code`) values 
(1,0,'https:\\/\\/tracking.mile.app\\/label\\/AWB00100209082020.1','2020-08-26 09:34:56','2020-08-26 09:34:59',9,0,NULL,0,'V WARP',NULL,'f70670b1-c3ef-4caf-bc4f-eefa702092ed',0,9,'e2cb6d86-0bb9-409b-a1f0-389ed4f2df2d',1,'AWB00100209082020.1'),
(2,0,'https:\\/\\/tracking.mile.app\\/label\\/AWB00100209082020.2','2020-08-26 09:36:32','2020-08-26 09:36:34',9,0,NULL,0,'V WARP',NULL,'f70670b1-c3ef-4caf-bc4f-eefa702092ed',0,9,'3600f10b-4144-4e58-a024-cc3178e7a709',1,'AWB00100209082020.2'),
(3,0,'https:\\/\\/tracking.mile.app\\/label\\/AWB00100209082020.3','2020-08-26 09:37:27','2020-08-26 09:37:29',2,0,NULL,0,'LID HOT CUP',NULL,'f70670b1-c3ef-4caf-bc4f-eefa702092ed',0,2,'2937bdbf-315e-4c5e-b139-fd39a3dfd15f',1,'AWB00100209082020.3');

/*Table structure for table `koli_surcharges` */

DROP TABLE IF EXISTS `koli_surcharges`;

CREATE TABLE `koli_surcharges` (
  `koli_surcharges_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `koli_surcharge` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`koli_surcharges_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `koli_surcharges` */

insert  into `koli_surcharges`(`koli_surcharges_id`,`koli_surcharge`,`created_at`,`updated_at`) values 
(1,'tes','2020-08-26 09:28:05','2020-08-26 09:28:07');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2020_08_25_1_create_current_locations_table',1),
(5,'2020_08_25_2_create_custom_fields_table',1),
(6,'2020_08_25_3_create_customer_attributes_table',1),
(7,'2020_08_25_4_create_koli_custom_fields_table',1),
(8,'2020_08_25_5_create_histories_table',1),
(9,'2020_08_25_6_create_customers_table',1),
(10,'2020_08_25_7_create_koli_surcharges_table',1),
(11,'2020_08_25_8_create_connotes_table',1),
(12,'2020_08_25_9_create_koli_datas_table',1),
(13,'2020_08_26_1_create_transaksis_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `transaksis` */

DROP TABLE IF EXISTS `transaksis`;

CREATE TABLE `transaksis` (
  `transaction_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customers_id` bigint(20) unsigned NOT NULL,
  `transaction_amount` int(11) NOT NULL DEFAULT 0,
  `transaction_discount` int(11) NOT NULL DEFAULT 0,
  `transaction_additional_field` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_payment_type` int(11) NOT NULL DEFAULT 0,
  `transaction_state` enum('PAID','UNPAID') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_order` int(10) unsigned NOT NULL,
  `location_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transaction_payment_type_name` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_cash_amount` int(11) NOT NULL DEFAULT 0,
  `transaction_cash_change` int(11) NOT NULL DEFAULT 0,
  `customer_attributes_id` int(10) unsigned NOT NULL,
  `origin_data_id` bigint(20) unsigned NOT NULL,
  `destination_data_id` bigint(20) unsigned NOT NULL,
  `custom_fields_id` int(10) unsigned NOT NULL,
  `current_locations_id` int(10) unsigned NOT NULL,
  `connotes_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `transaksis_customers_id_index` (`customers_id`),
  KEY `transaksis_customer_attributes_id_index` (`customer_attributes_id`),
  KEY `transaksis_origin_data_id_index` (`origin_data_id`),
  KEY `transaksis_destination_data_id_index` (`destination_data_id`),
  KEY `transaksis_custom_fields_id_index` (`custom_fields_id`),
  KEY `transaksis_current_locations_id_index` (`current_locations_id`),
  KEY `transaksis_connotes_id_index` (`connotes_id`),
  CONSTRAINT `transaksis_connotes_id_foreign` FOREIGN KEY (`connotes_id`) REFERENCES `connotes` (`connotes_id`),
  CONSTRAINT `transaksis_current_locations_id_foreign` FOREIGN KEY (`current_locations_id`) REFERENCES `current_locations` (`current_locations_id`),
  CONSTRAINT `transaksis_custom_fields_id_foreign` FOREIGN KEY (`custom_fields_id`) REFERENCES `custom_fields` (`custom_fields_id`),
  CONSTRAINT `transaksis_customer_attributes_id_foreign` FOREIGN KEY (`customer_attributes_id`) REFERENCES `customer_attributes` (`customer_attributes_id`),
  CONSTRAINT `transaksis_customers_id_foreign` FOREIGN KEY (`customers_id`) REFERENCES `customers` (`customers_id`),
  CONSTRAINT `transaksis_destination_data_id_foreign` FOREIGN KEY (`destination_data_id`) REFERENCES `customers` (`customers_id`),
  CONSTRAINT `transaksis_origin_data_id_foreign` FOREIGN KEY (`origin_data_id`) REFERENCES `customers` (`customers_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `transaksis` */

insert  into `transaksis`(`transaction_id`,`customers_id`,`transaction_amount`,`transaction_discount`,`transaction_additional_field`,`transaction_payment_type`,`transaction_state`,`transaction_code`,`transaction_order`,`location_id`,`organization_id`,`created_at`,`updated_at`,`transaction_payment_type_name`,`transaction_cash_amount`,`transaction_cash_change`,`customer_attributes_id`,`origin_data_id`,`destination_data_id`,`custom_fields_id`,`current_locations_id`,`connotes_id`) values 
('d0090c40-539f-479a-8274-899b9970bddc',3,70700,0,NULL,29,'PAID','CGKFT20200715121',121,'5cecb20b6c49615b174c3e74',6,'2020-08-26 09:39:36','2020-08-26 09:39:38','Invoice',0,0,1,1,2,1,1,'f70670b1-c3ef-4caf-bc4f-eefa702092ed'),
('d0090c40-539f-479a-8274-899b9970eggy',3,10000,2000,NULL,29,'UNPAID','BDG20200715126',122,'5cecb20b6c49615b174c3e74',6,'2020-08-26 07:23:04','2020-08-26 07:23:04','Retur',0,0,1,1,2,1,1,'f70670b1-c3ef-4caf-bc4f-eefa702092ed'),
('velit nulla',1,8932462,77478104,'laudantium magnam temporibus veritatis omnis nemo iste voluptatem autem voluptatem explicabo ullam cumque pariatur vitae commodi aut sit dolorum facere',66,'PAID','occaecati',983,'hic',6,'2020-08-26 10:35:40','2020-08-26 10:35:40','odit',52767266,72464492,1,1,1,1,1,'f70670b1-c3ef-4caf-bc4f-eefa702092ed');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Irving Jaskolski I','aida36@example.com','2020-08-26 09:45:14','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','diGUh7cxMz','2020-08-26 09:45:14','2020-08-26 09:45:14'),
(2,'Dorthy Jacobi','kunde.perry@example.com','2020-08-26 09:45:52','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','CxkrXCqMmq','2020-08-26 09:45:52','2020-08-26 09:45:52');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
