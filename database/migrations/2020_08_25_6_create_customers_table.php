<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigincrements('customers_id');
            $table->string('customers_name',30)->nullable();
            $table->string('customers_code',10)->nullable();
            $table->text('customer_address')->nullable();
            $table->string('customer_email',30)->nullable();
            $table->string('customer_phone',15)->nullable();
            $table->text('customer_address_detail')->nullable();
            $table->string('customer_zip_code',5)->nullable();
            $table->string('zone_code',3)->nullable();
            $table->integer('organization_id');
            $table->string('location_id',30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
