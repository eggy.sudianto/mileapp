<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKoliDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koli_datas', function (Blueprint $table) {
            $table->bigincrements('koli_datas_id');
            $table->integer('koli_length')->default(0);
            $table->text('awb_url')->nullable();
            $table->timestamps();
            $table->integer('koli_chargeable_weight')->default(0);
            $table->integer('koli_width')->default(0);
            $table->integer('koli_surcharges_id')->unsigned()->nullable();
            $table->index('koli_surcharges_id');
            $table->foreign('koli_surcharges_id')->references('koli_surcharges_id')->on('koli_surcharges');
            $table->integer('koli_height')->default(0);
            $table->text('koli_description')->nullable();
            $table->string('koli_formula_id',10)->nullable();
            $table->string('connotes_id',40)->nullable();
            $table->index('connotes_id');
            $table->foreign('connotes_id')->references('connotes_id')->on('connotes');
            $table->integer('koli_volume')->default(0);
            $table->integer('koli_weight')->default(0);
            $table->string('koli_id',40)->nullable();
            $table->integer('koli_custom_fields_id')->unsigned();
            $table->index('koli_custom_fields_id');
            $table->foreign('koli_custom_fields_id')->references('koli_custom_fields_id')->on('koli_custom_fields');
            $table->string('koli_code',20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koli_datas');
    }
}
