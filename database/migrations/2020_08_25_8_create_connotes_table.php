<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connotes', function (Blueprint $table) {
            $table->string('connotes_id',40)->primary();
            $table->integer('connote_number')->nullable();
            $table->string('connote_service',100)->nullable();
            $table->integer('connote_service_price')->default(0);
            $table->integer('connote_amount')->default(0);
            $table->string('connote_code',20)->nullable();
            $table->string('connote_booking_code',10)->nullable();
            $table->integer('connote_order')->unsigned();
            $table->enum('connote_state', ['PAID', 'UNPAID'])->nullable();
            $table->string('connote_state_id',2)->nullable();
            $table->string('zone_code_from',5)->nullable();
            $table->string('zone_code_to',3)->nullable();
            $table->string('surcharge_amount',10)->nullable();
            $table->string('transaction_id',40)->nullable();
            $table->integer('actual_weight')->default(0);
            $table->integer('volume_weight')->default(0);
            $table->integer('chargeable_weight')->default(0);
            $table->timestamps();
            $table->integer('organization_id')->unsigned();
            $table->string('location_id',30)->nullable();
            $table->integer('connote_total_package')->default(0);
            $table->integer('connote_surcharge_amount')->default(0);
            $table->integer('connote_sla_day')->default(0);
            $table->string('location_name',30)->nullable();
            $table->string('location_type',5)->nullable();
            $table->string('source_tariff_db',20)->nullable();
            $table->integer('id_source_tariff');
            $table->string('pod',10);
            $table->integer('histories_id')->unsigned()->nullable();
            $table->index('histories_id');
            $table->foreign('histories_id')->references('histories_id')->on('histories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connotes');
    }
}
