<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_attributes', function (Blueprint $table) {
            $table->increments('customer_attributes_id');
            $table->string('nama_sales',30)->nullable();
            $table->string('top',10)->nullable();
            $table->enum('jenis_pelanggan', ['B2B', 'B2C', 'C2C']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_attributes');
    }
}
