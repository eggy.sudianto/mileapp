<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->string('transaction_id',40)->primary();
            $table->biginteger('customers_id')->unsigned();
            $table->index('customers_id');
            $table->foreign('customers_id')->references('customers_id')->on('customers');
            $table->integer('transaction_amount')->default(0);
            $table->integer('transaction_discount')->default(0);
            $table->text('transaction_additional_field')->nullable();
            $table->integer('transaction_payment_type')->default(0);
            $table->enum('transaction_state', ['PAID', 'UNPAID'])->nullable();
            $table->string('transaction_code',20)->nullable();
            $table->integer('transaction_order')->unsigned();
            $table->string('location_id',30)->nullable();
            $table->integer('organization_id');
            $table->timestamps();
            $table->string('transaction_payment_type_name',10)->nullable();
            $table->integer('transaction_cash_amount')->default(0);
            $table->integer('transaction_cash_change')->default(0);
            $table->integer('customer_attributes_id')->unsigned();
            $table->index('customer_attributes_id');
            $table->foreign('customer_attributes_id')->references('customer_attributes_id')->on('customer_attributes');
            $table->biginteger('origin_data_id')->unsigned();
            $table->index('origin_data_id');
            $table->foreign('origin_data_id')->references('customers_id')->on('customers');
            $table->biginteger('destination_data_id')->unsigned();
            $table->index('destination_data_id');
            $table->foreign('destination_data_id')->references('customers_id')->on('customers');
            $table->integer('custom_fields_id')->unsigned();
            $table->index('custom_fields_id');
            $table->foreign('custom_fields_id')->references('custom_fields_id')->on('custom_fields');
            $table->integer('current_locations_id')->unsigned();
            $table->index('current_locations_id');
            $table->foreign('current_locations_id')->references('current_locations_id')->on('current_locations');
            $table->string('connotes_id',40);
            $table->index('connotes_id');
            $table->foreign('connotes_id')->references('connotes_id')->on('connotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
