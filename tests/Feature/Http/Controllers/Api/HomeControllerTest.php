<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Customer;
use App\Custom_field;
use App\Customer_attribute;
use App\CurrentLocation;
use Faker\Factory as Faker;

class HomeControllerTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function it_store_transaksi(){
        $customer = Customer::select("customers_id")->inRandomOrder()->first();
        $custom_field = Custom_field::select("custom_fields_id")->inRandomOrder()->first();
        $customer_attribute = Customer_attribute::select("customer_attributes_id")->inRandomOrder()->first();
        $currentLocation = CurrentLocation::select("current_locations_id")->inRandomOrder()->first();
        $faker = Faker::create('id_ID');

        $response = $this->post(route('package.store'), [
            'transaction_id' => $this->faker->words(2, true),
            'customers_id' => $customer->customers_id,
            'transaction_amount' => $faker->numberBetween($min = 1000, $max = 99999999),
            'transaction_discount' => $faker->numberBetween($min = 1000, $max = 99999999),
            'transaction_additional_field' => $this->faker->words(20, true),
            'transaction_payment_type' => $faker->numberBetween($min = 0, $max = 99),
            'transaction_state' => "PAID",
            'transaction_code' => $this->faker->words(1, true),
            'transaction_order' => $faker->numberBetween($min = 0, $max = 999),
            'location_id' => $this->faker->words(1, true),
            'organization_id' => 6,
            'transaction_payment_type_name' => $this->faker->words(1, true),
            'transaction_cash_amount' => $faker->numberBetween($min = 1000, $max = 99999999),
            'transaction_cash_change' => $faker->numberBetween($min = 1000, $max = 99999999),
            'customer_attributes_id' => $customer_attribute->customer_attributes_id,
            'origin_data_id' => $customer->customers_id,
            'destination_data_id' => $customer->customers_id,
            'custom_fields_id' => $custom_field->custom_fields_id,
            'current_locations_id' => $currentLocation->current_locations_id,
            'connotes_id' => $this->faker->words(2, true)
        ]);

        $response->assertStatus(200);
    }
}
